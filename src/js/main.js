// Main JS file

// DOM ready
$(function () {

    // Global
    var $body = $('body');

    // Nav toggle
    (function() {
        var $toggle = $('.nav-toggle'),
            $mainNav = $('.nav'),
            $sectionNav = $('.header__section-nav'),
            navOpenClass = 'nav-open';

        $toggle.on('click', function(e) {
            if ($body.hasClass(navOpenClass)) {
                toggleNav('close');
            } else {
                toggleNav('open');
            }

        });

        function toggleNav(state) {
            if (state === 'open') {
                $body.addClass(navOpenClass);
            }
            if (state === 'close') {
                $body.removeClass(navOpenClass);
            }
        }
    })();

    // Header search
    (function() {
        var $headerSearch = $('.header__search'),
            focusedClass = 'header__search--focused';

        $headerSearch.find('form').hover(
            function() {
                $headerSearch.addClass(focusedClass);
            }
        );
        $(document).mouseup(function (e) {
            var container = $('.' + focusedClass);
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.removeClass(focusedClass);
            }
        });
    })();

    // Move section menu to main nav on mobile
    function updateNavPosition() {
        var $mainNav = $('.header__main-nav'),
            $sectionNav = $('.header__section-nav'),
            $navMenu = $('.nav__menu'),
            $navToggle = $('.nav-toggle');

        if ($navToggle.is(':visible')) {
            $sectionNav
                .detach()
                .insertBefore($navMenu);
        } else {
            $sectionNav
                .detach()
                .insertAfter($mainNav);
        }
    }

    updateNavPosition();

    // Fancybox
    $('[data-fancybox]').fancybox({
        buttons: [
            'close'
        ],
        lang : 'cs',
        i18n : {
            'cs' : {
                CLOSE       : 'Zavřít',
                NEXT        : 'Další',
                PREV        : 'Předchozí',
                ERROR       : 'Požadovaný obsah se nepodařilo načíst. <br/> Zkuste to prosím znovu.',
                PLAY_START  : 'Spustit slideshow',
                PLAY_STOP   : 'Pozastavit slideshow',
                FULL_SCREEN : 'Celá obrazovka',
                THUMBS      : 'Náhledy'
            }
        }
    });

    // Init
    (function () {

    }());

    // Handle window resize
    $(window).on('resize', function (e) {
        updateNavPosition();
    });

    // Get screen size
    function screenSize() {
        var indicator = $('.screen-size:visible');
        return indicator.data('size');
    }

});
