'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var cssnext = require('postcss-cssnext');
var cleancss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var del = require('del');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var inline = require('gulp-inline');
var base64 = require('gulp-base64');
var sourcemaps = require('gulp-sourcemaps');
var inlineCss = require('gulp-inline-css');
var svgSprite = require("gulp-svg-sprites");

var paths = {
    lib: 'node_modules/',
    src: {
        html: 'src/html/',
        scss: 'src/scss/',
        js: 'src/js/',
        fonts: 'src/fonts/',
        images: 'src/img/',
        icons: 'src/icons/'
    },
    dist: {
        html: 'dist/',
        css: 'dist/css/',
        js: 'dist/js/',
        fonts: 'dist/css/fonts/',
        images: 'dist/img/',
        icons: 'dist/img/icons/'
    }
};

gulp.task('clean:dist', function () {
    return del([paths.dist + '**/*']);
});

gulp.task('clean:dist:css', function () {
    return del([paths.dist.css + '*.css']);
});

gulp.task('clean:dist:js', function () {
    return del([paths.dist.js]);
});

gulp.task('build:styles', function () {
    var plugins = [
        cssnext()
    ];
    return gulp.src(paths.src.scss + 'main.scss')
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(base64({
            baseDir: 'src',
            extensions: ['svg'],
            exclude: [],
            maxImageSize: 8 * 1024, // bytes
            debug: false
        }))
        .pipe(postcss(plugins))
        .pipe(gulp.dest(paths.dist.css))
        .pipe(cleancss({
            compatibility: 'ie9,-properties.merging'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.dist.css));
});

gulp.task('build:scripts', function () {
    return gulp.src(paths.src.js + 'main.js')
        .pipe(plumber())
        .pipe(gulp.dest(paths.dist.js))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.js));
});

gulp.task('concat:styles', function () {
    return gulp.src([
        paths.lib + 'normalize.css/normalize.css',
        paths.lib + '\@fancyapps/fancybox/dist/jquery.fancybox.css',
        paths.lib + 'slick-carousel/slick/slick.css',
        paths.lib + 'slick-carousel/slick/slick-theme.css',
        paths.dist.css + 'main.css'
    ])
        .pipe(plumber())
        .pipe(concat('main.package.css'))
        .pipe(cleancss({
            compatibility: 'ie9,-properties.merging'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.dist.css));
});

gulp.task('concat:scripts', function () {
    return gulp.src([
        paths.lib + 'jquery/dist/jquery.min.js',
        paths.lib + '\@fancyapps/fancybox/dist/jquery.fancybox.min.js',
        paths.lib + 'svgxuse/svgxuse.js',
        paths.lib + 'slick-carousel/slick/slick.min.js',
        paths.dist.js + 'main.js'
    ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.package.js'))
        .pipe(gulp.dest(paths.dist.js))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.js));
});

gulp.task('copy:fonts', function () {
    return gulp.src(paths.src.fonts + '**/*')
        .pipe(gulp.dest(paths.dist.fonts));
});

gulp.task('copy:images', function () {
    return gulp.src(paths.src.images + '**/*')
        .pipe(gulp.dest(paths.dist.images));
});

gulp.task('sprites:icons', function () {
    return gulp.src(paths.src.icons + '*.svg')
        .pipe(svgSprite({
            svg: {
                symbols: "icons.svg"
            },
            mode: "symbols",
            preview: false
        }))
        .pipe(gulp.dest(paths.dist.images));
});

gulp.task('inline:svg', function () {
    return gulp.src(paths.src.html + '*.html')
        .pipe(inline({
            base: 'src/',
            disabledTypes: ['css', 'js', 'img'], // Only inline svg files
            ignore: ['/img/logo.svg']
        }))
        .pipe(gulp.dest(paths.dist.html));
});

gulp.task('watch', function () {
    gulp.watch([paths.src.html + '**/*'], gulp.series('build:dist:html'));
    gulp.watch([paths.src.scss + '**/*'], gulp.parallel('build:dist:css'));
    gulp.watch([paths.src.js + '**/*'], gulp.series('build:dist:js'));
});

gulp.task('clean:dist', gulp.parallel('clean:dist'));

gulp.task('copy', gulp.parallel('copy:images', 'copy:fonts', 'sprites:icons'));

gulp.task('build:dist:html', gulp.parallel('inline:svg'));

gulp.task('build:dist:css', gulp.series('clean:dist:css', 'build:styles', 'concat:styles'));

gulp.task('build:dist:js', gulp.series('clean:dist:js', 'build:scripts', 'concat:scripts'));

gulp.task('build', gulp.parallel('build:dist:html', 'build:dist:css', 'build:dist:js', 'copy'));

gulp.task('default', gulp.series('clean:dist', 'build'));